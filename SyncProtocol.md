# SyncProtocol - Synchronizačná knižnica (Potrebná konzultácia ohľadne konkrétnych požadaviek)

iOS/macOS/Linux knižnica

## Definícia problému
Navrhnúť a implementovať dátový synchronizačný protokol. Protokol bude podporovať posielanie informácii o zmenách dát na server a bude schopný sťahovať zmeny zo serveru a vhodným spôsobom ich interpretovať. Protokol je určený pre správu objektových dát. Objekty môžu byť primitívne dátove typy (vrátane optional), pole alebo iný objekt

### Kostra správ
- Push: Protokol pushne lokálne zmeny na server
- Pull: Pullne zmeny zo servera a spojí ich s lokálnymi zmenami (treba vyriešiť konflikty)
- Login: Prihlásenie užívateľa

## Špecifikácia požiadaviek
- Sequence diagram protokolu
- Možnosť volaním metód poslať správy so zmenami (nové objekty, pozmenené objekty, zmazané objekty...)
- Možnosť spracovať interpretovať zmeny (napr. pomocou closure)
- Možnosť prihlásenia uživateľa (overenie identity)
- Protokol bude zasielať klientovi aj verziu dát

### Povolené knižnice
- [DataCompression](https://github.com/mw99/DataCompression) - kompresia dát
- [BlueSocket](https://github.com/IBM-Swift/BlueSocket) - Sockety
- [BlueSSLService](https://github.com/IBM-Swift/BlueSSLService) - SSL vrstva pre sockety
- **(Po dohode)** [Alamofire](https://github.com/Alamofire/Alamofire) - HTTP
