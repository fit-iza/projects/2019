# Aplikácia na držanie a správu hesiel

iOS alebo MacOS aplikácia

## Definícia problému
Užívateľ chce mať vlastnú aplikáciu na držanie hesiel priamo vo svojom zariadení. Chce mať možnosť pridávať, zobrazovať a filtrovať záznamy. Chce aby jeho heslá boli v bezpečí a teda pokiaľ sa neautentifikuje (faceID, touchID, pin) tak sa do aplikácie nedostane. Pri zobrazovaní záznamov sú heslá skryté (napr. ********) a na zobrazenie musí heslo odomknúť zadním pinu (tzn. ak si chce pozrieť 5 hesiel tak musí zadať 5 krát pin). Užívateľ chce mať možnosť zakázať prístup pomocou faceID alebo touchID a chce mať možnosť meniť pin.

## Špecifikácia požiadaviek
- Záznam môže mať atribúty
    - názov serveru/webstránky
    - username
    - email
    - heslo
    - poznámka
- Je možné pridať nový záznam
- Je možné záznamy filtrovať 
    - fitrovanie funguje pomocou regexu kde sa zobrazujú záznamy, ktoré obsahujú daný regex buď v názve serveru, username, email alebo poznámke **(v hesle sa nevyhľadáva)**
    - Užívateľ má možnosť určiť, v ktorých atribútoch sa bude vyhľadávať (implicitne vo všetkých)
- Atribúty záznamu je možné vhodne editovať
- Na vstup do aplikácie je potrebné sa autentifikovať (touchID, faceID ak je povolené alebo pin)
- Možnosť zmeniť pin
- Na odomknutie hesla je potrebné sa autentifikovať (touchID, faceID ak je povolené alebo pin)

## Povolené knižnice
- knižnica na správu faceID a touchID
