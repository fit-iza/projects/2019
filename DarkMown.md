# DarkMown - MarkDown editor

iOS aplikácia

## Definícia problému
Užívateľ si často potrebuje zapísať poznámky a chýba mu vhodný nástroj. Očakáva podporu pre jazyk markdown, možnosť exportu, importu. Užívateľ požaduje aby aplikácia vedela načítať .md súbor, ktorý má v aplikácii Files. Požaduje možnosť exportu dokumentov (hromadný export ním určených .md súborov) do aplikácie Files vo forme .md alebo .pdf. Pri písaní chce mať možnosť zobrazovať výsledok (live refresh). Nakoľko chce aplikáciu používať aj na iPhone tak musí byť možnosť vypnúť túto funkcionalitu priamo z editoru. Chce mať možnosť hintu, ktorá mu zobrazí základné pravidlá písania v markdowne (ako sa píše link, bold, italic a pod.).


## Špecifikácia požiadaviek
- Možnosť vytvoriť nový dokument
- Možnosť importovať dokument (Files)
- Možnosť exportovať dokument ako .md alebo .pdf (Files)
- Možnosť mazať lokálne dokumenty
- Možnosť editovať lokálne dokumenty
- Pri editovaní má možnosť zobraziť aktualnu verziu preloženého dokumentu.
- Má možnosť vybrať či bude mať na jednej strane obrazovky editor a na druhej výsledok alebo bude musieť prepínať medzi nimi
- Možnosť zobraziť hint s pravidlami jazyka MarkDown

## Povolené knižnice
- [NotePad](https://github.com/ruddfawcett/Notepad)
- [Marky-Mark](https://github.com/M2Mobi/Marky-Mark)
- [MarkdownView](https://github.com/keitaoouchi/MarkdownView)
- [MarkdownKit](https://github.com/bmoliveira/MarkdownKit)