# Trello client

iOS aplikácia

## Definícia problému

Užívateľ má veľa boardov v aplikácii Trello. Keď chce zistiť čo musí spraviť najskôr musí ručne každý board otvoriť a pozrieť sa do zoznamov (napr. To Do, Backlog) aby zistil, ktorý task má due date najbližší k dnešnému dňu. Vytvorte užívateľovi aplikáciu, ktorá mu tieto informácie dá na jednej obrazovke.

## Špecifikácia požiadaviek

Po zapnutí aplikácii sa zobrazia karty z Trella zoradené podľa due date alebo priority. Pre každú kartu sa zobrazia labele, popis a due date. 

- Po otvorení aplikácie sa stiahnú nové karty z nadefinovaných listov a boardov a zobrazia sa zoradené. 
- Užívateľ môže zadať interval, podľa ktorého sa karty prefiltrujú a ostanú iba tie, ktoré majú due date v intervale.
- Na karte sa okrem názvu zobrazujú aj labely a due date vhodne (napr. farebne) znázorňuje, či už bol due date, či je v priebehu 24 hodín alebo bude za viac ako 24 hodín.
- Užívateľ môže určiť viacej tabúľ (board) a viacej zoznamov (list), z ktorých sa karty získavajú.
- aplikácia funguje aj offline
- Možnosť otvoriť kartu v aplikácii Trello pomocou URL schema.

## Povolené knižnice
- [HelloTrello](https://github.com/livio/HelloTrello) - Trello wrapper (neoverená knižnica!)
- [Alamofire](https://github.com/Alamofire/Alamofire) - HTTP

## Zdroje
- [Trello doc](https://developers.trello.com)
- [trello url schemas](https://trello.com/c/cJfzOdDm/188-automate-with-url-scheme)

