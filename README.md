# Projekt 2019

V tomto repozitáry je uvedených niekoľko variant projektov do predmetu IZA na [VUT FIT v Brně](https://www.fit.vutbr.cz)

## Varianty

- [DarkMown](DarkMown.md) - iOS markdown editor **(0/3)**
- [Drink Water](Drink-Water.md) - iOS(iPhone)+watchOS aplikácia na pripomínanie pitného režimu **(3/3)**
- [Hungry Student](Hungry-Student.md) - iOS **(3/3)**/macOS **(0/3)** aplikácia na zobrazovanie jedla podľa surovín 
- [Password Holder](Password-Holder.md) - iOS **(3/3)**/macOS **(0/3)** aplikácia pre správu hesiel
- [SyncProtocol](SyncProtocol.md) - Knižnica pre synchronizáciu dát **(0/3)**
- [Trello Client](TrelloClient.md) - iOS aplikácia pre zobrazovanie kariet z Trella **(0/3)**

## Registrovanie variant
Registrácia variant prebieha pomocou emailu kde ako adresát bude uvedený mail filip@klembara.pro a do kópie vložte hrubym@fit.vutbr.cz. Dodržte formát predmetu správy **IZA project registration** a v správe uveďte o aký projekt máte záujem. Maximálny počet študentov na jeden projekt je **3**. Zmenu varianty alebo jej zrušenie riešte mailom s filip@klembara.pro. 

**Zaregistrovaním nejakej z týchto variant dávate súhlas so sprístupnením Vašeho riešenia mne ako osobe bez pracovného pomeru s VUT FIT.**

## Licencia
Vaše riešenie musí byť pod licenciou [MIT](https://choosealicense.com/licenses/mit/) alebo [apache 2](https://choosealicense.com/licenses/apache-2.0/). Súbor *LICENSE* s obsahom Vami zvolenej licencie odovzdajte spolu s projektom. Súbor by mal byť v rovnakom pričinku ako je *.xcodeproj*.

## Odovzdanie
Deadline projektov je určený vo wise. Tam odovzdáte aj svoje riešenia. Odovzdajte všetky potrebné súbory *(LICENSE, PROJECT\_NAME.xcodeproj, PROJECT\_NAME.xcworkspace, PROJECT\_NAME/ Pod/, Podfile.lock, Podfile apod...)*.

## Hodnotenie
Finálne hodnotenie je výhradne v rukách pána Hrubého, PhD. Ja môžem iba odporúčiť body. 

## Spoločné požiadavky

### Dokumentácia
Vypracujte jednoduchú dokumentáciu, v ktorej jednoducho popíšete ako výsledná aplikácia funguje, čo ste robili, aké ste mali problémy ich riešenia a čo nefunguje (priznanie nefungujúceho kódu je lepšie hodnotené ako odhalenie). Dokumentácie odovzdajte vo formáte *.pdf*. Nerozpisujte triviality a zbytočnosti, iOS je o jednoduchosti, tak to dodržte aj v dokumentácii.

### Dodržanie čistého kódu
- Použite nástroj [swiftlint](https://github.com/realm/SwiftLint)
- Dodržujte jednoduchosť aplikácie
- Nepíšte "1000" riadkové kontroléry
- Reálne používajte nejakú architektúru (MVC, MVVM, VIPER, ...) tak aby to bolo jasné už na prvý pohľad na kód
- Píšte konzistentné aplikácie (snažte sa neporušiť [HIG](https://developer.apple.com/design/human-interface-guidelines/))

### Knižnice tretích strán

Sú povolené knižnice tretích strán pokiaľ sa inštalujú pomocou [cocoapods](https://cocoapods.org). Použitie knižníc treba zvážiť nakoľko tento projekt nemá byť o tom, že si stiahnete knižnice na všetko a váš naimplementujete úplne minimum. Odporúčam použitie knižníc odôvodniť v dokumentácii. Zbytočné používanie knižníc je hodnotené negatívne. Pokiaľ nejaká varianta zadania má uvedené povolené knižnice tak to znamená, že ich použitie nemusíte odôvodniť.

### Databáza

Pre ukladanie dát využívajte [realm](https://realm.io/docs/swift/latest/) (CoreData výnimočne po dohode). Využívajte realm notifikácie. *(možno [RealmWrapper](https://github.com/k-lpmg/RealmWrapper))*

### Ďalšie požiadavky

- Pri iOS tabuľkách nutná podpora akcií pri horizontalnom swipe (aspoň delete)
- Podporované zariadenia (**Hrubo** zvýraznené musia byť bezkompromisne podporované)
    - iPhone
        - **iPhone SE**
        - **iPhone 8, 8 plus**
        - iPhone X
        - **iPhone Xs, Xs Max**
        - **iPhone Xr**
    - iPad
        - iPad Air (3rd gen)
        - **iPad Pro** (9.5", 12.5", **11"**, **12.9"**)


## Knižnice 
- [awesome-ios](https://github.com/vsouza/awesome-ios)

## Možné rozšírenia
- Localizácia