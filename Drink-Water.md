# Drink Water - Aplikácia pripomínajúca sa napiť

iOS (iPhone) + watchOS aplikácia

## Definícia problému
Užívateľ chce dodžiavať pitný režim a potrebuje niečo čo mu bude pripomínať aby sa napil. Chce mať aplikáciu, ktorá mu bude pravidelne posielať notifikácie s tým aby sa napil. Užívateľ chce mať možnosť nastaviť pravidelnosť notifikácii (napr. každých 60 minút) a dobu notifikácii (napríklad od 10:00-22:00 aby ho v noci nebudili). Užívateľ chce mať možnosť nastavovať denný goal koľko musí vypiť. Ak tento goal splní tak už nechce v daný deň dostávať ďalšie notifikácie. Z toho vyplýva, že požaduje možnosť zadávať koľko vody už vypil. Užívateľ ďalej požaduje aplikáciu na apple watch, z ktorej môže tak isto zadať koľko vody vypil. Nakoľko je vášnivý užívateľ aplikácie Health tak chce aby sa dáta ukladali do nej. Nepožaduje aplikáciu na iPad.

## Špecifikácia požiadaviek
- Aplikácia zasiela notifikácie užívateľovi aby sa napil
- Užívateľ má možnosť nastaviť ako často chodia notifikácie
- Užívateľ má možnosť nastaviť v akom období chodia notifikácie
- Možnosť zadať denný goal
- Možnosť pridávať objem vody koľko vypil na úvodnej obrazovke
- Možnosť pridávať objem vody z apple watch
- Dáta sa ukladajú do aplikácie Health

## Zdroje
- [HealthKit](https://developer.apple.com/documentation/healthkit)
