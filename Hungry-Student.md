# Hungry Student - Aplikácia zobrazujúca jedlá na základe surovín

iOS alebo MacOS aplikácia

## Definícia problému
Užívateľ nie je kreatívny a tak chce aplikáciu, ktorá mu na základe surovín aké má zobrazí aké jedlá si môže spraviť. Chce mať možnosť pridávať do aplikácie rôzne jedlá a priradiť k ním suroviny. Chce mať možnosť vyhľadávať v jedlách pomocou surovín, ktoré si vie takisto pridávať ako samostatné položky. Pri vyhľadávaní chce aby sa zobrazovali jedlá, ktoré majú aspoň jednu určenú surovinu v sebe alebo jedlá, ktoré neobsahujú žiadnu inú surovinu. Chce mať možnosť si ku jedlu pridať poznámku alebo recept. Ďalej chce aby mal možnosť si jedlá hodnotiť (napr. 0-5 hviezdičiek) a pri zobrazovaní jedál budú najskôr zobrazené jedlá s väčším počtom hviezdičiek. Užívateľ trpí komplexom fotenia jedla a tak chce mať možnosť pridať ku jedlu jeho fotku vyfotením priamo z aplikácie alebo vybratím z galérie.

## Špecifikácia požiadaviek
- Možnosť prídávať suroviny
- Možnosť pridávať jedlá a naviazať suroviny ku ním
- Možnosť mazať suroviny a jedlá
- Možnosť vybrať suroviny do vyhľadávania (keďže surovín je veľa tak má možnosť filtrovať suroviny podľa podreťazca (case-insensitive))
- Na zákalde typu vyhľadávania sa buď zobrazia jedlá, ktoré 
    - majú aspoň jednu zo zadaných surovín
    - obsahujú iba zadané suroviny
- Možnosť pridať poznámku alebo recept ku jedlu
- Možnosť hodnotiť jedlá
- Lepšie hodnotené jedlá sa budú zobrazovať pred menej hodotenými
- Možnosť pridať obrázok jedla vyfotením priamo z aplikácie alebo nahratím z galérie (macOS varianta iba nahratím z galérie)

